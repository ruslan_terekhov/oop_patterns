<?php

class BookProduct extends ShopProduct
{
    public $pagesCount;

    /**
     * BookProduct constructor.
     * @param $pagesCount
     */
    public function __construct($title, $producerMainName, $producerFirstName, $price, $pagesCount)
    {
        parent::__construct($title, $producerMainName, $producerFirstName, $price);
        $this->pagesCount = $pagesCount;
    }

      public function getPagesCount()
    {
        return $this->pagesCount;
    }

    public function getSummaryLine()
    {
        $base = parent::getSummaryLine();
        $base   .=": Количество страниц - {$this->getPagesCount()}";

        return $base;
    }


}