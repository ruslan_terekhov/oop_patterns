<?php

class ShopProductWriter
{
    protected $products = [];

    public function addProduct(ShopProduct $shopProduct)
    {
        array_push($this->products, $shopProduct);
    }

    public function write()
    {
        $str = "";
        foreach ($this->products as $shopProduct) {
            $str .="{$shopProduct->getTitle()}: ";
            $str .= $shopProduct->getProducer();
            $str .="({$shopProduct->getPrice()})<br>";
        }

        print $str;
    }
}