<?php

class CDProduct extends ShopProduct
{
    public $playLength;

    /**
     * CDProduct constructor.
     * @param $playLength
     */
    public function __construct($title, $producerMainName, $producerFirstName, $price, $playLength)
    {
        parent::__construct($title, $producerMainName, $producerFirstName, $price);
       $this->playLength = $playLength;
    }

    public function getPlayLength()
    {
        return $this->playLength;
    }


    public function getSummaryLine()
    {
        $base = parent::getSummaryLine();
        $base   .=": Время звучания - {$this->playLength}";

        return $base;
    }

}