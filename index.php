<?php
include 'autoloader.php';
$dsn = "sqlite:/vagrant/db/store.db";
$pdo = new PDO($dsn, null, null);
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$bookProduct = new BookProduct(
    'PHP Programming',
    'Zandstra',
    'Matt',
    99.99,
    577
    );

$cd = new CDProduct(
    'The Greatest Hits',
    'Frank',
    'Sinatra',
    50.57,
    180
);
$obj = ShopProduct::getInstance(1, $pdo);
$obj2 = ShopProduct::getInstance(2, $pdo);

$bookProduct->setDiscount(51);
$writer = new ShopProductWriter();
$writer->addProduct($bookProduct);
$writer->addProduct($cd);
$writer->addProduct($obj);
$writer->addProduct($obj2);
$writer->write();
