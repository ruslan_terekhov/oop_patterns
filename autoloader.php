<?php
function __autoload($className) {
    $fileName = $className.'.php';
    if (file_exists($fileName)) {
        include $fileName;
    } else {
        throw new \Exception('Class file not found');
    }

}